package com.example.gamer.homework5.AddUniversityData;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.gamer.homework5.CalledBack.CallBackUniversity;
import com.example.gamer.homework5.Entity.DataSchool;
import com.example.gamer.homework5.R;

import static android.app.Activity.RESULT_OK;

public class AddUniverAdapter extends DialogFragment {

    CallBackUniversity.AddDialogUniversity addDialogUniversity;
    public static final int PICK_IMAGE=100;
    Uri imageUri;

    ImageView imageView;
    @Override
    public void onAttach(Context context) {


        super.onAttach(context);
        addDialogUniversity= (CallBackUniversity.AddDialogUniversity) context;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
        View view=LayoutInflater.from(getActivity()).inflate(R.layout.add_data_un,null);
        builder.setTitle("Add New University");
        builder.setView(view);
        final MyViewHolder myViewHolder=new MyViewHolder(view);


        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DataSchool dataSchool=new DataSchool(myViewHolder.etName.getText().toString(),
                                                     "+"+myViewHolder.etPhone.getText().toString(),
                                                     myViewHolder.etWeb.getText().toString(),
                                                     myViewHolder.etEmail.getText().toString(),
                                                     myViewHolder.etLocation.getText().toString(),
                                                     imageUri
                                                    );
                addDialogUniversity.getUniversity(dataSchool);
            }
        });
        return builder.create();
    }



    class MyViewHolder{

        EditText etName,etPhone,etEmail,etWeb,etLocation;

        Button btnPickImage;


        public MyViewHolder(View view){
            etName=view.findViewById(R.id.et_un_name);
            etPhone=view.findViewById(R.id.et_un_phone);
            etWeb=view.findViewById(R.id.et_un_web);
            etEmail=view.findViewById(R.id.et_un_email);
            etLocation=view.findViewById(R.id.et_un_address);
            imageView=view.findViewById(R.id.choose_img);
            btnPickImage=view.findViewById(R.id.btn_pick_image);

            btnPickImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openGallery();
                }
            });


        }
    }



    private void openGallery(){
        Intent gallery=new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery,PICK_IMAGE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK&&requestCode==PICK_IMAGE){
            imageUri=data.getData();
            imageView.setImageURI(imageUri);

        }
    }
}
