package com.example.gamer.homework5.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gamer.homework5.CalledBack.CallBackUniversity;
import com.example.gamer.homework5.CalledBack.ItemClickListener;
import com.example.gamer.homework5.Entity.DataSchool;
import com.example.gamer.homework5.MainActivity;
import com.example.gamer.homework5.R;

import java.util.List;

public class AdapterUniversity extends RecyclerView.Adapter<AdapterUniversity.MyViewHolder> {

    List<DataSchool> dataSchoolList;
    Context context;
    LayoutInflater inflater;
    CallBackUniversity callBackUniversity;



    public AdapterUniversity(List<DataSchool> dataSchoolList, Context context) {
        this.dataSchoolList = dataSchoolList;
        this.context = context;
        inflater=LayoutInflater.from(context);
        callBackUniversity= (CallBackUniversity) context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
      View view=inflater.inflate(R.layout.university_list,viewGroup,false);
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        DataSchool dataSchool=dataSchoolList.get(i);
        myViewHolder.tvName.setText(dataSchool.getUnName());
        myViewHolder.tvPhone.setText(dataSchool.getUnPhone());
        myViewHolder.tvWeb.setText(dataSchool.getUnWeb());
        myViewHolder.tvEmail.setText(dataSchool.getUnEmail());
        myViewHolder.tvLocation.setText(dataSchool.getUnAddress());
        myViewHolder.imageView.setImageURI(dataSchool.getPictsure());
        myViewHolder.inSideImage.setImageURI(dataSchool.getPictsure());


        myViewHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, final int position, boolean isLongClick) {

                if (isLongClick){
                    AlertDialog.Builder builder=new AlertDialog.Builder(context);
                    View mView=LayoutInflater.from(view.getContext()).inflate(R.layout.add_data_un,null);

                    builder.setTitle("Update University Data");
                    final TextView txtName=mView.findViewById(R.id.et_un_name);
                    final TextView txtPhone=mView.findViewById(R.id.et_un_phone);
                    final TextView txtWeb=mView.findViewById(R.id.et_un_web);
                    final TextView txtEmail=mView.findViewById(R.id.et_un_email);
                    final TextView txtAddress=mView.findViewById(R.id.et_un_address);
                    final  ImageView img =mView.findViewById(R.id.ivUniversity);


                    txtEmail.setText(dataSchoolList.get(position).getUnName());
                    txtPhone.setText(dataSchoolList.get(position).getUnPhone());
                    txtWeb.setText(dataSchoolList.get(position).getUnWeb());
                    txtEmail.setText(dataSchoolList.get(position).getUnEmail());
                    txtAddress.setText(dataSchoolList.get(position).getUnAddress());
//                    img.setImageURI(dataSchoolList.get(position).getPictsure());

                    builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dataSchoolList.get(position).setUnName(txtName.getText().toString());
                                    dataSchoolList.get(position).setUnName(txtPhone.getText().toString());
                                    dataSchoolList.get(position).setUnName(txtWeb.getText().toString());
                                    dataSchoolList.get(position).setUnName(txtEmail.getText().toString());
                                    dataSchoolList.get(position).setUnName(txtAddress.getText().toString());

                                }
                            });

                    builder.setView(mView);
                    builder.show();

                            Toast.makeText(context, "Long Click!" + dataSchoolList.get(position), Toast.LENGTH_SHORT).show();
                }



            }
        });


    }

    @Override
    public int getItemCount() {
        return dataSchoolList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener{

        ImageView imageView,inSideImage;
        TextView tvName,tvPhone,tvWeb,tvEmail,tvLocation;
        Button btnDelete;
        private ItemClickListener itemClickListener;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            imageView=itemView.findViewById(R.id.ivUniversity);
            tvName=itemView.findViewById(R.id.tv_un_name);
            tvPhone=itemView.findViewById(R.id.tv_un_phone);
            tvWeb=itemView.findViewById(R.id.tv_un_web);
            tvEmail=itemView.findViewById(R.id.tv_un_email);
            tvLocation=itemView.findViewById(R.id.tv_un_address);
            btnDelete=itemView.findViewById(R.id.btn_delete);
            inSideImage=itemView.findViewById(R.id.iv_in_University);

            tvWeb.setMovementMethod(LinkMovementMethod.getInstance());

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DataSchool dataSchool=dataSchoolList.get(getAdapterPosition());
                    callBackUniversity.delete(dataSchool,getAdapterPosition());
                }
            });

            tvWeb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                    browserIntent.setData(Uri.parse("http://"+tvWeb.getText().toString()));
                    context.startActivity(browserIntent);
                }
            });

        }


        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v,getAdapterPosition(),false);
        }

        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v,getAdapterPosition(),true);
            return true;
        }
    }





}
